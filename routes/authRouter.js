const express = require('express');
const { MongoClient } = require('mongodb');
const debug = require('debug')('app:auth');

const authRouter = express.Router();
const url = 'mongodb://localhost:27017';
const dbName = 'sunshine';
const passport = require('passport');

router = (nav) => {
  authRouter.route('/signup')
    .get((req, res) => {
      res.render('signup', { navItems: nav });
    });

  authRouter.route('/signup')
    .post((req, res) => {
      const mongodb = async () => {
        try {
          const { username, password } = req.body;
          const dbUser = { username, password };
          client = await MongoClient.connect(url);
          const db = client.db(dbName);
          const dbresult = await db.collection('user')
            .insertOne(dbUser);
          req.login(dbresult.ops[0], () => res.redirect('/auth/profile'));
        } catch (error) {

        }
      };
      mongodb();
    });

  authRouter.route('/profile')
  			.all((req,res,next) => {
  				if(req.user){next();}
  				else{ res.redirect('/')}
			})
  			.get((req, res) => {
    			res.json(req.user);
  });

  authRouter.route('/signin')
    .get((req, res) => { res.render('signin', { navItems: nav, title: 'Sign In' }); })
    .post(passport.authenticate('local', {
      successRedirect: '/auth/profile',
      failureRedirect: '/',
    }));

     authRouter.route('/signout')
    .get((req, res) => { req.logout(); res.redirect('/'); });    
  return authRouter;
};





module.exports = router;
