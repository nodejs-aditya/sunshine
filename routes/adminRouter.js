const express = require('express');

const adminRouter = express.Router();
const { MongoClient } = require('mongodb');
const debug = require('debug')('app:admin');

router = (nav) => {
  adminRouter.route('/')
    .all((req,res, next) =>{   // allow authenticate user only
      if(req.user) {next();} 
      else{res.redirect('/');}
    })
    .get((req, res) => {
      const url = 'mongodb://localhost:27017';
      const dbName = 'sunshine';

      (
        async function mongo() {
          let clinet;
          try {
            client = await MongoClient.connect(url);
            const db = client.db(dbName);
            const response = await db.collection('products')
              .insertMany(
                [
                  { item: 'card', qty: 15 },
	      				{ item: 'envelope', qty: 20 },
	      				{ item: 'stamps', qty: 30 },
   					],
              );

            res.json(response);
            client.close();
          } catch (error) {
            debug(error.stack);
          }
        }()
      );
    });
  return adminRouter;
};

const sampleProducts =	[
	  { name: 'Light Fixtures 4x4', brand: 'Black Capsule' },

];

module.exports = router;
