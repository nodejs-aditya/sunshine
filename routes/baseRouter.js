const express = require('express');

const baseRouter = express.Router();

const router = (nav) => {
  baseRouter.route('/').get((req, res) => {
    res.render('index', {
      navItems: nav,
      title: 'Sunshine',
      user: req.user
    });
  });
  return baseRouter;
};

module.exports = router;
