const express = require('express');
const productService = require('../service/ProductService');

const productsRouter = express.Router();
const debug = require('debug')('app:productRouter');

router = (nav) => {
  productsRouter.route('/').get((req, res) => {
    productService.getProducts(res, nav, renderProductList);
  });

  productsRouter.route('/:id').get((req, res) => {
    const { id } = req.params;
    debug('Getting single product - %s', id);
    productService.getProductById(res, id, renderProduct);
  });

  return productsRouter;
};

renderProduct = (error, res, product) => {
  res.render('singleproduct', {
    product,
    navItems: nav,
  });
};

renderProductList = (error, nav, res, productList) => {
  if (error) {
    productsList = {};
    debug('Error in productsRouter: %s ', error);
  }
  res.render('products', {
    products: productList,
    navItems: nav,
  });
};

module.exports = router;
