const passport=  require('passport');
const localStrategy = require('passport-local').Strategy;
const MongoConfig = require('../database/MongoConfig.js');
const { MongoClient } = require('mongodb');
const debug = require('debug')('app:localstrategy');

const Strategy = ()=>{
	passport.use(
		new localStrategy(
			{usernameField:'username',passwordField: 'password'},
			(username,password, done) => 
			{
				let client;
				const findUser = async () => {
					try{
						console.log('In ');
						client = await MongoClient.connect('mongodb://localhost:27017');
						const db  = client.db('sunshine');	
						const user = await db.collection('user')
							.findOne({'username':username});//short cut json
						debug(user);	
						if(user.password===password){
							console.log('Password match');
							console.log(user);
							done(null, user); // params: error, user
						} else{
							console.log('Password does not match');
							done(null, false);
						}							
						
					} catch(error){
						console.log(error.stack);
					}
					client.close();
				};
				findUser();	
			}
		));
};

module.exports=Strategy;