const passport =  require('passport');
require('./strategy/local.strategy')();

passportConfig = app => {

	app.use(passport.initialize());
	app.use(passport.session());

	// store to session
	passport.serializeUser((user,done) => {
		done(null, user);
	});

	//retrieve from session
	passport.deserializeUser((user, done)=> {
		done(null, user);
	});
};

module.exports = passportConfig; 