const express = require('express');
const chalk = require('chalk');
const morgan = require('morgan'); // console.log endpoints
const debug = require('debug')('app');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const sql = require('mssql');
const database = require('./util/mssqldatabase.js');
const navigationService = require('./service/NavigationService');

// Route
const baseRouter = require('./routes/baseRouter')(navigationService());
const productsRouter = require('./routes/productsRouter')(navigationService());
const adminRouter = require('./routes/adminRouter')(navigationService());
const authRouter = require('./routes/authRouter')(navigationService());

const blueLog = (param) => debug(chalk.blue.underline(param));
const redLog = (param) => debug(chalk.red.underline(param));

sql.connect(database.config).catch((err) => redLog(err));

const port = process.env.PORT || 3000;

const app = express();
app.use(session({ secret: 'sunshine' }));
require('./util/passport')(app);

app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/css', express.static(path.join(__dirname, '/node_modules/bootstrap/dist/css')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/jquery/dist')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());
app.use('/', baseRouter);
app.use('/auth', authRouter);
app.use('/products', productsRouter);
app.use('/admin', adminRouter);

app.set('views', './views');
app.set('view engine', 'ejs');

app.listen(port, () => {
  blueLog(`Listening on port: ${port}`);
});
