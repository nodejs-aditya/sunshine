const sql = require('mssql');
const debug = require('debug')('app:productService');

const getProducts = (res, nav, callback) => {
  const request = new sql.Request();
  request.query('select id, name, brand from sfproduct')
    .then((result) => {
      debug(result);
      callback(null, nav, res, result.recordset);
    }).catch((err) => {
      callback(new Error(`Error occurred >> ${err}`));
    });
};

const getProductById = async (res, id, callback) => {
  const request = new sql.Request();
  const result = await request
    .input('id', sql.Int, id)
    .query('select id,name, brand from sfproduct where id=@id');

  debug(result);
  callback(null, res, result.recordset[0]);
};

module.exports = {
  getProducts, getProductById,
};
