const getNavItems = () => {
  const navItems = [
    { name: 'About',    link: '/about' },
    { name: 'Products', link: '/products' },
    { name: 'Profile',  link: '/auth/profile'},
    { name: 'Signin',   link: '/auth/signin' },
    { name: 'Signup',   link: '/auth/signup' },
    { name: 'Signout',  link: '/auth/signout' },
  ];
  return navItems;
};

module.exports = getNavItems;
